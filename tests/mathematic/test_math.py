from time import sleep


def test_math_still_works():
    sleep(2)
    assert 1 + 1 == 2


def test_incorrect_calculation():
    assert 2 > 5
